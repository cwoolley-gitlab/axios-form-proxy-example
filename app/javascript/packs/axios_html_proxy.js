import axios from 'axios'

const AxiosHtmlProxy = () => {
  document.addEventListener('turbolinks:load', () => {
    if (!document.querySelector('.issues.new')) {
      return
    }
    
    listenForSubmit()
  })
}

const listenForSubmit = () => {
  // attach to the form
  const form = document.querySelector('form')
  // listen for the submit event
  // form.addEventListener('submit', submitWithAxios)
  form.addEventListener('submit', submitWithFetch)
}

const submitWithAxios = (e) => {
  e.preventDefault()
  
  const form = e.target
  const formData = new FormData(form)

  // NOTE: This does not work, because the redirect has already been followed,
  //  and even if there is a form validation error, the response will be
  //  a 200 and the error will be embedded in the text of the response HTML.
  
  // You cannot (currently) prevent axios from following the redirect,
  // issues asking for this have been closed without comment:
  // - https://github.com/axios/axios/issues/932#issuecomment-533408837
  
  // An option may be to use native `fetch` instead, which CAN prevent the
  // redirect from being followed, with the `redirect: manual` parameter:
  // - https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/fetch#parameters
  // This seems to work in this simple case, see below.
  return (
    axios({
      method: 'post',
      url: form.action,
      data: formData,
      maxRedirects: 0,
      // headers: { 'Content-Type': 'multipart/form-data' },
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    })
    .then((successResponse) => {
      window.location = successResponse.request.responseURL
    })
    .catch((errorResponse) => {
      window.location = errorResponse.request.responseURL
    })
  )
}

const submitWithFetch = async (e) => {
  e.preventDefault()

  const form = e.target
  const formData = new FormData(form)
  const csrfToken = document.querySelector("[name='csrf-token']").content
  
  // See https://shahata.medium.com/why-i-wont-be-using-fetch-api-in-my-apps-6900e6c6fe78
  const response = await fetch(form.action, {
    method: 'post',
    credentials: 'include',
    mode: 'cors',
    headers: {
      "X-CSRF-Token": csrfToken,
      // 'Accept': contentType,
      // 'Content-Type': contentType, // Not needed: https://stackoverflow.com/a/45840405/25192
    },
    body: formData,
    redirect: 'manual',
  })
  .then((response) => {
    // See https://fetch.spec.whatwg.org/#concept-filtered-response-opaque-redirect
    if (response.type == 'opaqueredirect') {
      window.location = response.url
      return // is this necessary? Does window.location stop execution?
    }

    debugger
    
    // If we didn't get a redirect, return the response so we can extract and set
    // the document html out of the body.
    // See https://stackoverflow.com/a/64070046/25192
    return response
  })
  .catch((error) => {
    // TODO: how to handle? With fetch, this is only network errors, not all non-200
    const msg = 'Unexpected error:'
    console.log(msg, response)
    alert(msg + response)
  })

  debugger
  
  // Extract the html from the response text and set it as the document body
  // PROBLEM: BUT - the events are not hooked up. E.g., the next form submission
  // will not have the submit event hooked up properly.
  const documentText = await response.text();
  document.open();
  document.write(documentText);
  document.close();

}

export default AxiosHtmlProxy