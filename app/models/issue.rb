class Issue < ApplicationRecord
  # Allow "faking" an error by giving issue a title matching /error/
  validates_format_of :title, without: /error/
end
